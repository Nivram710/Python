import RPi.GPIO as GPIO
import time
import random

rot = 12
gruen = 32

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(rot, GPIO.OUT)
GPIO.setup(gruen, GPIO.OUT)

GPIO.output(rot, GPIO.LOW)
GPIO.output(gruen, GPIO.LOW)

passwort = "1234"

eingabe = input("Passwort: ")

if eingabe == passwort:
    print("Login erfolgreich!")
    for i in range(3):
        GPIO.output(gruen, GPIO.HIGH)
        time.sleep(0.1 + random.random() / 5)
        GPIO.output(gruen, GPIO.LOW)
        time.sleep(random.uniform(0.1, 0.5))
    
else:
    print("Login fehlgeschlagen!")
    for i in range(3):
        GPIO.output(rot, GPIO.HIGH)
        time.sleep(random.choice([0.5, 1]))
        GPIO.output(rot, GPIO.LOW)
        time.sleep(random.choice([0.5, 1]))
